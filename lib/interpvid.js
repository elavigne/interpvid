var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

require('video.js/dist/video-js.min.css');

var _ = require('lodash');
var React = require('react');
var moment = require('moment');
var VideoJS = require('./vendor/video.js');

var Card = require('material-ui/lib/card');
var FlatButton = require('material-ui/lib/flat-button');
var RadioButton = require('material-ui/lib/radio-button');
var RadioButtonGroup = require('material-ui/lib/radio-button-group');
var TextField = require('material-ui/lib/text-field');

var modalDataPropTypes = React.PropTypes.shape({
    timecode: React.PropTypes.oneOfType([// When to display during video
    React.PropTypes.number, // Number in seconds
    React.PropTypes.instanceOf(moment.duration) // A moment duration object
    ]),
    title: React.PropTypes.node, // The node displayed at the top of the popup
    contentText: React.PropTypes.node,
    choices: React.PropTypes.arrayOf(React.PropTypes.string),
    contentType: React.PropTypes.oneOf(['shortAnswer', 'multipleChoice']),
    contentAfter: React.PropTypes.node // Shown after a response is given (i.e., feedback)
});

var TextModalContent = React.createClass({
    displayName: 'TextModalContent',

    getData: function () {
        return this.props;
    },

    render: function () {
        return React.createElement(
            'div',
            null,
            this.props.contentText
        );
    }
});

var ShortAnswerModalContent = React.createClass({
    displayName: 'ShortAnswerModalContent',

    getData: function () {
        return _.defaults({
            answer: this.refs.textArea.getValue()
        }, this.props);
    },

    render: function () {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                { style: { display: 'table-row' } },
                React.createElement(
                    'label',
                    { style: { color: 'rgba(0,0,0,0.5)', display: 'table-cell', paddingRight: 5 } },
                    'Question:'
                ),
                React.createElement(
                    'div',
                    { style: { display: 'table-cell' } },
                    this.props.contentText
                )
            ),
            React.createElement(TextField, { ref: 'textArea', fullWidth: true, multiLine: true,
                floatingLabelText: 'Answer:' })
        );
    }
});

var MultipleChoiceModalContent = React.createClass({
    displayName: 'MultipleChoiceModalContent',

    getData: function () {
        return _.defaults({
            answer: this.refs.radioGroup.getSelectedValue()
        }, this.props);
    },

    render: function () {
        var choices = _.map(this.props.choices, function (choice) {
            return React.createElement(RadioButton, { key: choice, value: choice, label: choice,
                style: { marginTop: 14 } });
        });
        return React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                { style: { display: 'table-row' } },
                React.createElement(
                    'label',
                    { style: { color: 'rgba(0,0,0,0.5)', display: 'table-cell', paddingRight: 5 } },
                    'Question:'
                ),
                React.createElement(
                    'div',
                    { style: { display: 'table-cell' } },
                    this.props.contentText
                )
            ),
            React.createElement('br', null),
            React.createElement(
                'label',
                { style: { color: 'rgba(0,0,0,0.5)', marginLeft: 2 } },
                'Answer:'
            ),
            React.createElement(
                RadioButtonGroup,
                { name: 'choices', ref: 'radioGroup' },
                choices
            )
        );
    }
});

var VideoModal = React.createClass({
    displayName: 'VideoModal',

    propTypes: {
        modalData: modalDataPropTypes,
        onSubmitModal: React.PropTypes.func,
        isLastOfSet: React.PropTypes.bool
    },

    getInitialState: function () {
        return {
            savedData: null,
            showContentAfter: false
        };
    },

    _nextInQueue: function () {
        var currentItem = this.state.modalDataQueue[0];
        if (currentItem && currentItem.contentAfter) {}

        this.setState({
            modalDataQueue: _.slice(this.state.modalDataQueue, 1),
            savedData: (this.state.savedData || []).concat(this.refs.cardContent.getData())
        }, _.bind(function () {
            if (this.state.modalDataQueue.length == 0) {
                this.props.onSubmitModal(_.cloneDeep(this.state.savedData));
                return this.setState({
                    savedData: []
                });
            }
        }, this));
    },

    _finishHandler: function () {
        if (this.state.showContentAfter === false && !_.isNull(this.props.modalData.contentAfter) && !_.isUndefined(this.props.modalData.contentAfter)) {
            return this.setState({
                showContentAfter: true
            });
        }

        this.setState({
            showContentAfter: false
        });

        this.props.onSubmitModal(this.state.savedData);
    },

    _saveData: function () {
        this.setState({
            savedData: this.refs.cardContent.getData()
        }, this._finishHandler);
    },

    render: function () {
        if (_.isUndefined(this.props.modalData) || _.isNull(this.props.modalData)) {
            return React.createElement('div', null);
        }

        var cardActionHandler = this._finishHandler;

        var buttonText = 'Next';

        var content = null;
        if (this.state.showContentAfter === true) {
            buttonText = 'Ok';
            content = React.createElement(
                'div',
                null,
                this.props.modalData.contentAfter
            );
        } else {
            cardActionHandler = this._saveData;

            var CardContentClass = TextModalContent;
            if (this.props.modalData.contentType == 'shortAnswer') {
                CardContentClass = ShortAnswerModalContent;
            } else if (this.props.modalData.contentType == 'multipleChoice') {
                CardContentClass = MultipleChoiceModalContent;
            }

            content = React.createElement(CardContentClass, _extends({}, this.props.modalData, { ref: 'cardContent' }));

            buttonText = this.props.isLastOfSet == true ? 'Submit' : buttonText;
        }

        return React.createElement(
            Card.Card,
            null,
            React.createElement(Card.CardTitle, { title: this.props.modalData.title }),
            React.createElement(
                Card.CardText,
                null,
                content
            ),
            React.createElement(
                Card.CardActions,
                { style: { textAlign: 'right' } },
                React.createElement(FlatButton, { ref: 'submitButton', label: buttonText, onClick: cardActionHandler })
            )
        );
    }
});

var InterpVideo = React.createClass({
    displayName: 'InterpVideo',

    propTypes: {
        enableSeeking: React.PropTypes.bool,
        modalData: React.PropTypes.objectOf(modalDataPropTypes),
        onVideoEnd: React.PropTypes.func,
        src: React.PropTypes.string
    },

    getInitialState: function () {
        var modalData = {};

        _.each(this.props.modalData, function (vals, key) {
            if (_.isNumber(vals.timecode)) {
                vals.timecode = moment.duration(vals.timecode, 'seconds');
            }
            vals.timecode = vals.timecode.asMilliseconds();
            vals.id = key;

            var modalDataAtTC = _.get(modalData, vals.timecode, []);
            modalDataAtTC.push(vals);
            modalData[vals.timecode] = modalDataAtTC;
        });

        _.each(modalData, _.sortBy);

        return {
            lastTimecode: 0,
            videoPlayer: null,
            modalData: modalData,
            modalElBounds: {},
            savedData: {}
        };
    },

    _seekingHandler: function () {
        // Disable seeking
        if (this.props.enableSeeking !== true) {
            var delta = this.state.videoPlayer.currentTime() - this.currentTime;
            if (Math.abs(delta) > 0.01) {
                this.state.videoPlayer.currentTime(this.currentTime);
            }
        }
    },

    _getVideoPlayerComponent: _.memoize(function () {
        return React.createElement(VideoJS, _.defaultsDeep(this.props.videoOptions, {
            key: 'videoPlayer',
            refs: 'videoPlayer',
            src: this.props.src,
            options: { autoplay: false },
            resize: true,
            eventListeners: {
                loadstart: this._loadstartHandler,
                timeupdate: this._timeupdateHandler,
                ended: this._endedHandler,
                seeking: this._seekingHandler
            } }));
    }),

    _loadstartHandler: function (evt) {
        var modalDialog = evt.target.player.createModal(this.refs.videoModalContainer, {
            temporary: false,
            uncloseable: true
        });
        modalDialog.close();

        this.setState({
            videoPlayer: evt.target.player,
            modalDialog: modalDialog
        });
    },

    _timeupdateHandler: function (evt) {
        if (evt.target.player.scrubbing_ === false) {
            this.currentTime = this.state.videoPlayer.currentTime();
        }

        var momentCurrentTime = moment.duration(this.currentTime, 'seconds');

        _.each(_.keys(this.state.modalData), _.bind(function (tc) {
            if (momentCurrentTime.asMilliseconds() >= parseFloat(tc)) {
                this._createModal(tc);
            }
        }, this));
    },

    _createModal: function (tc) {
        var modalData = this.state.modalData;
        var modalDataAtTC = modalData[tc];

        if (_.isNull(modalDataAtTC) || _.isUndefined(modalDataAtTC) || _.isArray(modalDataAtTC) && modalDataAtTC.length == 0) {
            return this.setState({
                modalData: _.omit(modalData, tc)
            });
        }

        this.setState({
            modalData: modalData,
            modalDataQueue: modalDataAtTC,
            currentTC: tc
        }, _.bind(function () {
            this.state.modalDialog.open();
        }, this));
    },

    _modalSubmitHandler: function (data) {
        var newData = {};
        newData[data.id] = _.omit(data, 'id');

        this.setState({
            savedData: _.assign(newData, this.state.savedData),
            modalData: this.state.modalDataQueue.length <= 1 ? _.omit(this.state.modalData, this.state.currentTC) : this.state.modalData,
            modalDataQueue: _.rest(this.state.modalDataQueue)
        }, function () {
            if (_.isEmpty(this.state.modalDataQueue)) {
                this.state.modalDialog.close();
            }
        });
    },

    _endedHandler: function () {
        if (_.isFunction(this.props.onVideoEnd)) {
            this.props.onVideoEnd(this.state.savedData);
        }
    },

    render: function () {
        var modalContainerStyle = {
            width: '100%',
            verticalAlign: 'middle',
            display: 'inline-block'
        };

        return React.createElement(
            'div',
            {className: 'interpvid' },
            this._getVideoPlayerComponent(),
            React.createElement(
                'div',
                { style: modalContainerStyle, ref: 'videoModalContainer' },
                React.createElement(VideoModal, { modalData: _.first(this.state.modalDataQueue),
                    isLastOfSet: _.size(this.state.modalDataQueue) == 1,
                    onSubmitModal: this._modalSubmitHandler })
            )
        );
    }
});

module.exports = {
    InterpVideo: InterpVideo
};