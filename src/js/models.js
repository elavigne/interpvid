'use strict'

var backbone = require('backbone')
var moment = require('moment')

var models = {}

models.ModalModel = backbone.Model.extend({
    name: 'ModalModel',
    defaults: {
        id: null,
        index: 0,
        timecode: 0,
        title: 'Probe',
        contentText: '',
        contentType: 'text',
        contentAfter: '',
        startTimestamp: 0,
        submitTimestamp: 0,
        endTimestamp: 0,
        choices: [],
        answer: null,
        presented: false
    },
    getTimecode: function() {
        var tc = this.get('timecode')

        if (_.isNumber(tc) || _.isString(tc)) {
            tc = moment.duration(parseInt(tc), 'seconds')
        }

        return tc.asMilliseconds()
    }
})

models.ModalCollection = backbone.Collection.extend({
    name: 'ModalCollection',
    model: models.ModalModel,
    comparator: 'index'
})


module.exports = models