require('../../node_modules/video.js/dist/video-js.min.css');

var _ = require('lodash')
var React = require('react')
var moment = require('moment')
var VideoJS = require('./vendor/video.js')

var Card = require('material-ui/lib/card')
var FlatButton = require('material-ui/lib/flat-button')
var RadioButton = require('material-ui/lib/radio-button')
var RadioButtonGroup = require('material-ui/lib/radio-button-group')
var TextField = require('material-ui/lib/text-field')
var Toggle = require('material-ui/lib/toggle')

var models = require('./models')

var TextModalContent = React.createClass({
    render: function () {
        return (
            <div style={{whiteSpace: 'pre'}}>
                {this.props.model.get('contentText')}
            </div>
        )
    }
})

var NumberModalContent = React.createClass({
    _handleOnChange: function() {
        this.props.model.set('answer', this.refs.textField.getValue())

        if (!this.props.model.get('changeTimestamp')) {
            this.props.model.set('changeTimestamp', Date.now())
        }
    },

    render: function () {
        var textFieldProps = {
            ref: 'textField',
            fullWidth: true,
            type: 'number',
            onChange: this._handleOnChange,
            onBlur: this._handleOnChange,
            disabled: this.props.model.get('preferNotToAnswer') === true,
            floatingLabelText: 'Answer:'
        }

        if (this.props.model.get('preferNotToAnswer') === true) {
            textFieldProps.value = ''
            this.props.model.set('answer', '')
        }

        return (
            <div>
                <div style={{display: 'table-row'}}>
                    <label style={{color: 'rgba(0,0,0,0.5)', display: 'table-cell', paddingRight: 5}}>Question:</label>
                    <div style={{display: 'table-cell'}}>
                        {this.props.model.get('contentText')}
                    </div>
                </div>
                <TextField {...textFieldProps} />
            </div>
        )
    }
})

var ShortAnswerModalContent = React.createClass({
    _handleOnChange: function() {
        this.props.model.set('answer', this.refs.textArea.getValue())

        if (!this.props.model.get('changeTimestamp')) {
            this.props.model.set('changeTimestamp', Date.now())
        }
    },

    render: function () {
        var textFieldProps = {
            ref: 'textArea',
            fullWidth: true,
            multiLine: true,
            onChange: this._handleOnChange,
            onBlur: this._handleOnChange,
            disabled: this.props.model.get('preferNotToAnswer') === true,
            floatingLabelText: 'Answer:'
        }

        if (this.props.model.get('preferNotToAnswer') === true) {
            textFieldProps.value = ''
            this.props.model.set('answer', '')
        }

        return (
            <div>
                <div style={{display: 'table-row'}}>
                    <label style={{color: 'rgba(0,0,0,0.5)', display: 'table-cell', paddingRight: 5}}>Question:</label>
                    <div style={{display: 'table-cell'}}>
                        {this.props.model.get('contentText')}
                    </div>
                </div>
                <TextField {...textFieldProps}/>
            </div>
        )
    }
})

var MultipleChoiceModalContent = React.createClass({

    _handleOnChange: function(evt, value) {
        this.props.model.set('answer', value)

        if (!this.props.model.get('changeTimestamp')) {
            this.props.model.set('changeTimestamp', Date.now())
        }
    },

    render: function () {
        var choices = _.map(this.props.model.get('choices'), _.bind(function(choice) {
            return (
                <RadioButton key={choice} value={choice} label={choice}
                             disabled={this.props.model.get('preferNotToAnswer') === true}
                             style={{marginTop: 14}} />
            )
        }, this))

        var buttonGroupProps = {
            name:'choices',
            ref:'radioGroup',
            onChange:this._handleOnChange
        }

        if (this.props.model.get('preferNotToAnswer') === true) {
            buttonGroupProps.valueSelected = null
            this.props.model.set('answer', null)
        }

        return (
            <div>
                <div style={{display: 'table-row'}}>
                    <label style={{color: 'rgba(0,0,0,0.5)', display: 'table-cell', paddingRight: 5}}>Question:</label>
                    <div style={{display: 'table-cell'}}>
                        {this.props.model.get('contentText')}
                    </div>
                </div>
                <br />
                <label style={{color: 'rgba(0,0,0,0.5)', marginLeft: 2}} >Answer:</label>
                <RadioButtonGroup {...buttonGroupProps}>
                    {choices}
                </RadioButtonGroup>
            </div>
        )
    }
})


var MultipleNumericModalContent = React.createClass({

    _handleOnChange: function(evt) {
        var answer = ''
        _.each(this.props.model.get('choices'), _.bind(function(choice, index) {
            answer += (this.refs[index].getValue() || '') + (index < this.props.model.get('choices').length - 1 ? '$' : '')
        }, this))

        this.props.model.set('answer', answer)

        if (!this.props.model.get('changeTimestamp')) {
            this.props.model.set('changeTimestamp', Date.now())
        }
    },

    render: function () {
        var choices = _.flatten(_.map(this.props.model.get('choices'), _.bind(function(choice, index) {
            return [
                (
                    <TextField key={choice}
                               ref={index}
                               type="number"
                               data-index={index}
                               floatingLabelText={choice}
                               disabled={this.props.model.get('preferNotToAnswer') === true}
                               onChange={this._handleOnChange}
                               onBlur={this._handleOnChange}
                               fullWidth
                               style={{marginTop: 14}} />
                ),
                <br />
            ]
        }, this)))

        if (this.props.model.get('preferNotToAnswer') === true) {
            this.props.model.set('answer', null)
        }

        return (
            <div>
                <div style={{display: 'table-row'}}>
                    <label style={{color: 'rgba(0,0,0,0.5)', display: 'table-cell', paddingRight: 5}}>Question:</label>
                    <div style={{display: 'table-cell'}}>
                        {this.props.model.get('contentText')}
                    </div>
                </div>
                <br />
                <label style={{color: 'rgba(0,0,0,0.5)', marginLeft: 2}} >Answer:</label>
                <br/>
                {choices}
            </div>
        )
    }
})

var VideoModal = React.createClass({
    propTypes: {
        model: React.PropTypes.object,
        onSubmitModal: React.PropTypes.func,
        isLastOfSet : React.PropTypes.bool
    },

    getInitialState: function() {
        return {
            savedData: null,
            showContentAfter: false
        }
    },

    _finishHandler: function() {
        this._submitTimestamp = Date.now()

        var contentAfter = this.props.model.get('contentAfter')
        if (this.state.showContentAfter === false && !_.isNull(contentAfter) &&
                !_.isUndefined(contentAfter) && contentAfter != '') {
            return this.setState({
                showContentAfter: true
            })
        }

        this.setState({
            showContentAfter: false
        })


        this.props.onSubmitModal(this.props.model, this._submitTimestamp)
    },

    _handlePreferNotToAnswer: function(evt, value) {
        this.props.model.set('preferNotToAnswer', value)
        this.forceUpdate()
    },

    render: function () {
        if (_.isUndefined(this.props.model) || _.isNull(this.props.model)) {
            return <div/>
        }

        var buttonText = 'Next'

        var content = null
        if (this.state.showContentAfter === true) {
            buttonText = 'Ok'
            content = (
                <div style={{whiteSpace: 'pre'}}>
                    {this.props.model.get('contentAfter')}
                </div>
            )
        } else {
            var CardContentClass = TextModalContent
            if (this.props.model.get('contentType') == 'shortAnswer') {
                CardContentClass = ShortAnswerModalContent
            } else if (this.props.model.get('contentType') == 'multipleChoice') {
                CardContentClass = MultipleChoiceModalContent
            } else if (this.props.model.get('contentType') == 'multipleNumeric') {
                CardContentClass = MultipleNumericModalContent
            } else if (this.props.model.get('contentType') == 'number') {
                CardContentClass = NumberModalContent
            }

            if (this.props.model.get('showPreferNotToAnswer') === true) {
                content = (
                    <div>
                        <CardContentClass model={this.props.model} ref="cardContent"/>
                        <br />
                        <Toggle onToggle={this._handlePreferNotToAnswer}
                                label={'Prefer Not To Answer'}
                                labelPosition="right" />
                    </div>
                )
            } else {
                content = (
                    <CardContentClass model={this.props.model} ref="cardContent"/>
                )
            }


            buttonText = (this.props.isLastOfSet == true) ? 'Submit' : buttonText
        }

        return (
            <Card.Card>
                <Card.CardTitle title={this.props.model.get('title')}/>
                <Card.CardText>
                    {content}
                </Card.CardText>
                <Card.CardActions style={{textAlign: 'right'}}>
                    <FlatButton ref="submitButton" label={buttonText}
                                onClick={this._finishHandler}/>
                </Card.CardActions>
            </Card.Card>
        )
    }
})

var InterpVideo = React.createClass({
    propTypes: {
        enableSeeking: React.PropTypes.bool,
        enablePausing: React.PropTypes.bool,
        collection: React.PropTypes.object,
        onVideoEnd: React.PropTypes.func,
        onMute: React.PropTypes.func,
        onPlay: React.PropTypes.func,
        src: React.PropTypes.string
    },

    getInitialState: function () {

        var collection = {}
        if (this.props.collection) {
            var collection = this.props.collection.groupBy(function(m) {
                return m.getTimecode()
            })
            _.each(collection, _.sortBy)
        }

        var resultsCollection = new models.ModalCollection()

        this._probeStartTime = 0

        return {
            lastTimecode: 0,
            videoPlayer: null,
            collection: collection,
            modalElBounds: {},
            resultsCollection: resultsCollection
        }
    },

    componentWillMount: function() {
        this._getVideoPlayerComponent = _.memoize(this._getVideoPlayerComponent)
    },

    _seekingHandler: function() {
        // Disable seeking
        if (this.props.enableSeeking !== true) {
            var delta = this.state.videoPlayer.currentTime() - this.currentTime
            if (Math.abs(delta) > 0.01) {
                this.state.videoPlayer.currentTime(this.currentTime)
            }
        }
    },
    
    getVideoPlayer: function() {
        return this.refs.videoPlayer.getVideoPlayer()
    },

    _getVideoPlayerComponent: function () {
        var options = {
            autoplay: false,
            controlBar: {
                playToggle: this.props.enablePausing === true
            }
        }
        return (
            <VideoJS
                {...this.props.videoOptions}
                key="videoPlayer"
                ref="videoPlayer"
                src={this.props.src}
                options={options}
                resize={true}
                eventListeners={{
					 	loadstart: this._loadstartHandler,
					 	play: this._playHandler,
					 	timeupdate: this._timeupdateHandler,
					 	volumechange: this._muteHandler,
					 	pause: this._pauseHandler,
					 	ended: this._endedHandler,
					 	seeking: this._seekingHandler
					 }}/>
        )
    },

    _loadstartHandler: function (evt) {
        var modalDialog = evt.target.player.createModal(this.refs.videoModalContainer, {
            temporary: false,
            uncloseable: true
        })
        modalDialog.close()

        this.setState({
            videoPlayer: evt.target.player,
            modalDialog: modalDialog
        })
    },

    _muteHandler: function(evt) {
        if (_.isFunction(this.props.onMute)) {
            this.props.onMute(evt)
        }
    },

    _playHandler: function(evt) {
        if (_.isFunction(this.props.onPlay)) {
            this.props.onPlay(evt)
        }
    },

    _timeupdateHandler: function (evt) {
        if (evt.target.player.scrubbing_ === false) {
            this.currentTime = this.state.videoPlayer.currentTime()
        }

        var momentCurrentTime = moment.duration(this.currentTime, 'seconds')

        _.each(_.keys(this.state.collection), _.bind(function (tc) {
            if (momentCurrentTime.asMilliseconds() >= parseFloat(tc)) {
                this._createModal(tc)
            }
        }, this))

    },

    _pauseHandler: function(evt) {
        evt.preventDefault()
    },

    _createModal: function (tc) {
        var collection = this.state.collection
        var collectionAtTC = collection[tc]

        if (_.isNull(collectionAtTC) || _.isUndefined(collectionAtTC) ||
            (_.isArray(collectionAtTC) && collectionAtTC.length == 0)) {
            return this.setState({
                collection: _.omit(collection, tc)
            })
        }

        this.setState({
            collection: collection,
            modelQueue: collectionAtTC,
            currentTC: tc
        }, _.bind(function () {
            if (this.state.modelQueue.length > 0) {
                this._probeStartTime = Date.now()
            }
            this.state.modalDialog.open()
        }, this))
    },

    _modalSubmitHandler: function (model, submitTimestamp) {
        let newModel = this.state.resultsCollection.add(model.clone())
        newModel.set({
            startTimestamp: this._probeStartTime,
            submitTimestamp: submitTimestamp,
            endTimestamp: Date.now()
        })

        this.setState({
            collection: this.state.modelQueue.length <= 1
                ? _.omit(this.state.collection, this.state.currentTC) : this.state.collection,
            modelQueue: _.rest(this.state.modelQueue)
        }, function() {
            if (_.isEmpty(this.state.modelQueue)) {
                this.state.modalDialog.close()
            }

        })

    },

    _endedHandler: function() {
        if (_.isFunction(this.props.onVideoEnd)) {
            this.props.onVideoEnd(this.state.resultsCollection)
        }
    },

    render: function () {
        var modalContainerStyle = {
            width: '100%',
            verticalAlign: 'middle',
            display: 'inline-block'
        }

        return (
            <div className="interpvid">
                {this._getVideoPlayerComponent()}
                <div style={modalContainerStyle} ref="videoModalContainer">
                    <VideoModal model={_.first(this.state.modelQueue)}
                                isLastOfSet={_.size(this.state.modelQueue) == 1}
                                onSubmitModal={this._modalSubmitHandler}/>
                </div>
            </div>

        )
    }
});

module.exports = {
    InterpVideo: InterpVideo,
    VideoModal: VideoModal
};