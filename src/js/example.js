require.ensure([
    'less/main.less',
], function (require) {
    require('less/main.less');

    var React = require('react');
    var ReactDOM = require('react-dom');
    var interpvid = require('./index.js');

    var App = React.createClass({
        render () {
            var modalData = {
                1: {timecode: 1, title: 'test 2', contentText: 'test me', contentType: 'shortAnswer',
                    contentAfter: 'this is me testing feedback'},
                2: {
                    timecode: 7, title: 'test 2', contentText: 'test me', contentType: 'multipleChoice',
                    choices: ['a', 'b', 'c', 'd']
                }
            }



            return (
                <div style={{width: '100%', height: '100%'}}>
                    <interpvid.InterpVideo modalData={modalData}
                        src="http://mirror.cessen.com/blender.org/peach/trailer/trailer_400p.ogg"/>
                </div>
            );
        }
    });

    ReactDOM.render(<App />, document.getElementById('view'));
});



